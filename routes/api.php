<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
// use Response;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\VideoApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::get('/', function () {

//     $result = ['result' => 'OK',
//                'data' => 'No Data Yet'];
  
//     $response = Response::json($result);
  
//     return $response;
  
// });

Route::post('/login', [ApiController::class,'userLogin'])->name("login");
Route::post('/register', [ApiController::class,'userRegister'])->name("register");
Route::post('/forgotpass', [ApiController::class,'ForgotPass'])->name("forgotpass");
Route::post('/getvideo', [VideoApiController::class,'getVideo'])->name("getVideo");
Route::get('/getallvideo', [VideoApiController::class,'getAllVideo'])->name("getAllVideo");
Route::post('/getlikes', [VideoApiController::class,'getLikes'])->name("getLikes");
Route::post('/getcomments', [VideoApiController::class,'getComments'])->name("getComments");


Route::group([
    'middleware' => 'auth.jwt',

], function ($router) {
    Route::post('/changepass', [ApiController::class,'changePass'])->name("changepass");

    Route::post('/edit', [ApiController::class,'userEdit'])->name("edit");
    Route::post('/delete', [ApiController::class,'userDelete'])->name("delete");
    Route::post('/logout', [ApiController::class,'userLogout'])->name("logout");
    

    Route::post('/fileupload', [VideoApiController::class,'FileUpload'])->name("fileupload");
    Route::post('/uservideo', [VideoApiController::class,'getUsersVideo'])->name("getUsersVideo");
    
    Route::post('/setlikes', [VideoApiController::class,'setLikes'])->name("setLikes");
    Route::post('/setcomments', [VideoApiController::class,'setComments'])->name("setComments");

    Route::post('/setcommentflag', [VideoApiController::class,'setCommentFlag'])->name("setCommentFlag");
    Route::post('/setvideoflag', [VideoApiController::class,'setVideoFlag'])->name("setVideoFlag");
});