<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@getEmail')->name('password.request');
// Route::post('forget-password', 'App\Http\Controllers\Auth\ForgotPasswordController@postEmail')->name('password.email');

// Route::get('reset-password/', 'App\Http\Controllers\Auth\ResetPasswordController@getPassword')->name('resetPass');
// Route::post('update-password/', 'App\Http\Controllers\Auth\ResetPasswordController@updatePassword');


Route::prefix('admin')->group(function () {
    Route::get('/', [App\Http\Controllers\AdminController::class, 'index'])->name('admin');
    Route::get('users', [App\Http\Controllers\AdminController::class, 'getusers'])->name('getusers');
    Route::get('vidoes', [App\Http\Controllers\AdminController::class, 'getvideos'])->name('getvideos');
    Route::get('comments', [App\Http\Controllers\AdminController::class, 'getcomments'])->name('getcomments');
    Route::post('changeuserstatus', [App\Http\Controllers\AdminController::class, 'changeuserStatus'])->name('changeUserStatus');
    Route::post('changecommentstatus', [App\Http\Controllers\AdminController::class, 'changeCommentStatus'])->name('changeCommentStatus');
    Route::post('changevideostatus', [App\Http\Controllers\AdminController::class, 'changevideoStatus'])->name('changeVideoStatus');
    Route::get('video/{id}', [App\Http\Controllers\AdminController::class, 'videoDetails'])->name('videoDetails');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();
