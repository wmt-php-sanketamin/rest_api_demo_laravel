<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = [
        'body',
        'user_id',
        'video_id'
    ];

    public function video()
    {
        return $this->belongsTo('App\Models\Video', 'video_id','id');
    }
}
