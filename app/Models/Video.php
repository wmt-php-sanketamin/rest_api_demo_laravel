<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'video_path'
    ];

    public function author()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function vote()
    {
        return $this->hasMany('App\Models\VoteCount', 'video_id' , 'id');
    }
    public function comments()
    {
        return $this->hasMany('App\Models\Comment', 'video_id' , 'id');
    }
    
    public function reports()
    {
        return $this->hasMany('App\Models\ReportVideo', 'video_id' , 'id');
    }
}
