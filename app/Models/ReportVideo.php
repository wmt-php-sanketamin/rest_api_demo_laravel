<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportVideo extends Model
{
    use HasFactory;
    protected $fillable = [
        'video_id',
        'user_id',
        'status'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User', 'id' , 'user_id');
    }

    public function videos()
    {
        return $this->hasOne('App\Models\Video', 'id' , 'video_id');
    }
}
