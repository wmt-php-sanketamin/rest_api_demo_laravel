<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReportComment extends Model
{
    use HasFactory;
    protected $fillable = [
        'comment_id',
        'user_id',
        'status'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User', 'user_id','id');
    }
}
