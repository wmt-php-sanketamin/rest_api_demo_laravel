<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoteCount extends Model
{
    use HasFactory;
    // protected $table = 'vote_counts';

    protected $fillable = [
        'vote',
        'user_id',
        'video_id'
    ];

    public function video()
    {
        return $this->belongsTo('App\Models\Video', 'video_id');
    }
}
