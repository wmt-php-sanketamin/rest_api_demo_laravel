<?php

namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\ApiResponse;
use App\Http\Requests\AppConstant;

class CreateApiUserRequest extends FormRequest
{
    use ApiResponse;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'confirm_password'  => 'required|same:password'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name is required!',
            'email' => [
                'required' => 'We need to know your email address!',
                'email' => 'Your email address syntax is wrong'
            ],
            'password' => [
                'required' => 'Password is required!',
                'min:6' => 'Your password should be minimum 6 digit long'
            ],
            'confirm_password' => [
                'required' => 'Confirm_Password is required!',
                'same:password' => 'Confirm_password should be same as password'
            ],
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $this->setMeta('message', $validator->messages()->first());
        $this->setMeta('status', false);
        $response = new JsonResponse( $this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        throw (new ValidationException($validator, $response))->status(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
