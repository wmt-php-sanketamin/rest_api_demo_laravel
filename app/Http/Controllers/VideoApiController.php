<?php

namespace App\Http\Controllers;
use TymonJWTAuthExceptionsJWTException;
use Hash;
use Validator;
use JWTAuth;
use DB;

use App\Models\User;
use App\Models\Video;
use App\Models\VoteCount;
use App\Models\Comment;
use App\Models\ReportComment;
use App\Models\ReportVideo;

use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use App\Jobs\SendVerifyEmail;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response; 
use Carbon\Carbon;


class VideoApiController extends Controller
{
    use ApiResponse;

    public function FileUpload(Request $request)
    {
        try {
            // $validated = $request->validated();

            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];
            
            $user = User::find($u_id);

            if(!$user){
                $this->setMeta('message', __('messages.user.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            } 

            if($request->hasFile('url')){
                
                $file = $request->file('url');
                $file_name = $file->getClientOriginalName();
                $path = $request->file('url')->storeAs(
                    'public/videos',$file_name
                );
            }

            $video = new Video;
            $video->video_path = 'storage/videos/'.$file_name;
            $video->VideoName = $file_name;
            $video->user_id = $u_id;
    
            $video->save();
            
            $this->setMeta('message', __('messages.video.upload'));
            $this->setMeta('url', '/storage/videos/'.$file_name);
            $this->setMeta('status', AppConstant::STATUS_OK);

            return response()->json($this->setResponse(),AppConstant::OK);

        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
    }

    public function getUsersVideo(Request $request)
    { 
        try{
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];

            $user_video = User::with(['video'  => function($query){
                $query->where('status',1);
            }])->where('id',$u_id)->get();

            $this->setData('User_Video', $user_video);
            $this->setData('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function getVideo(Request $request){
        try{
            $v_id = $request->video_id;

            $video = Video::with('author')->where(['id' => $v_id, 'status' => 1])->first();
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }
            $this->setData('Video', $video);
            $this->setData('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function getAllVideo()
    {
        try{
            $video = Video::with(['author','comments'  => function($query){
                $query->where('status',1);
            },'vote' => function($query){
                $query->where('vote',1);
            }])->withCount(['comments'  => function($query){
                $query->where('status',1);
            },'vote' => function($query){
                $query->where('vote',1);
            }])->where('status',1)->get();

            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $this->setMeta('message', __('messages.video.allVideos'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('All Videos', $video);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }

    }

    public function setLikes(Request $request)
    {
        try{
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];

            $v_id = $request->video_id;
            $vote = $request->status;

            $video = Video::where(['id' => $v_id, 'status' => 1])->first();
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $ex1 = VoteCount::updateOrCreate([
                'user_id' =>  $u_id,
                'video_id' => $v_id
            ],[
                'vote' => $vote
            ],
            );

            $this->setMeta('message', __('messages.voteSuccess'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);


        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }

    }

    public function getLikes(Request $request)
    {
        try{

            $v_id = $request->video_id;

            $video = Video::where(['id' => $v_id, 'status' => 1])->first();
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $ex = Video::with(['vote' => function($query){
                $query->where('vote',1);
            }])->withCount(['vote' => function($query){
                $query->where('vote',1);
            }])->where('id', $v_id)->first();
        
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('Video', $ex);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function setComments(Request $request)
    {
        try{
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];

            $v_id = $request->video_id;
            $comment = $request->comment;

            $video = Video::where(['id' => $v_id, 'status' => 1])->first();
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $add_comment = new Comment;
            $add_comment->user_id = $u_id;
            $add_comment->video_id = $v_id;
            $add_comment->body = $comment;

            $add_comment->save();

            $this->setMeta('message', __('messages.comment.commentSuccess'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function getComments(Request $request)
    {
        try{

            $v_id = $request->video_id;

            $video = Video::withCount(['comments' => function($query){
                $query->where('status',1);
            }])->with(['comments' => function($query){
                $query->where('status',1);
            }])->where(['id' => $v_id, 'status' => 1])->first();
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }
        
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('Video', $video);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function setCommentFlag(Request $request)
    {
        try{
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];

            $c_id = $request->comment_id;
            $status = $request->status;

            $comment = Comment::find($c_id);
            if(!$comment){
                $this->setMeta('message', __('messages.comment.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $report_comment = ReportComment::updateOrCreate([
                'comment_id' =>  $c_id,
                'user_id' => $u_id
            ],[
                'status' => $status
            ],
            );

            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('Message', __('messages.updateFlag'));
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

    public function setVideoFlag(Request $request)
    {
        try{
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $u_id = $token_data['id'];

            $v_id = $request->video_id;
            $status = $request->status;

            $video = Video::find($v_id);
            if(!$video){
                $this->setMeta('message', __('messages.video.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }

            $report_comment = ReportVideo::updateOrCreate([
                'video_id' =>  $v_id,
                'user_id' => $u_id
            ],[
                'status' => $status
            ],
            );

            $this->setMeta('Message', __('messages.updateFlag'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
        }
    }

}