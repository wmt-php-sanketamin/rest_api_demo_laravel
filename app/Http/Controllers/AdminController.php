<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Video;
use App\Models\Comment;
use App\Models\ReportVideo;
use App\Utils\AppConstant;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function adminLoginForm()
    {
        // dd("hello");
        return view('admin.adminlogin');
    }

    public function index()
    {
        return view('admin.adminDashboard');
    }

    public function getusers(Request $request)
    {
        if ($request->ajax()) {
            $users = User::query();
            return Datatables::of($users)
            ->addColumn('action', function(User $user) {
                if($user->status == 1)
                {
                    return '<button type="button" class="btn btn-secondary m-0" onclick="UserSwal(\''.$user->id.'\') ">Deactivate</button>';
                }
                else{
                    return '<button type="button" class="btn btn-secondary m-0" onclick="UserSwal(\''.$user->id.'\') ">Activate</button>';
                }
            })
            ->make(true);
        }
        return view('admin.userdetails');

    }

    public function getvideos(Request $request)
    {
        if ($request->ajax()) {
            $videos = Video::query();
            return Datatables::of($videos)
            ->addColumn('action', '<a class="btn btn-secondary btn-sm pr-4" href="{{route(\'videoDetails\',$id )}}" role="button"><i class="fas fa-video"></i> View Details</a>')
            ->make(true);
        }
        return view('admin.allvideos');
    }

    public function getcomments(Request $request)
    {
        if ($request->ajax()) {
            $comments = Comment::query();
            return Datatables::of($comments)
            ->addColumn('action', function(Comment $comment) {
                if($comment->status == 1)
                {
                    return '<button type="button" class="btn btn-secondary m-0" onclick="CommentSwal(\''.$comment->id.'\') ">Deactivate</button>';
                }
                else{
                    return '<button type="button" class="btn btn-secondary m-0" onclick="CommentSwal(\''.$comment->id.'\') ">Activate</button>';
                }
            })
            ->make(true);
        }
        return view('admin.allcomments');

        $comments = Comment::all();
        return view('admin.allcomments',compact('comments'));
    }

    public function videoDetails($id)
    {
        $reported = Video::withCount(['vote'  => function($query){
            $query->where('status',1);
        },'comments'  => function($query){
            $query->where('status',1);
        }])->with(['reports' => function($query){
            $query->where('status', AppConstant::STATUS_INACTIVE );
        }])->where( 'id', $id )->first();

        return view('admin.videodetails',compact('reported'));
    }
    
    public function changeuserStatus(Request $request)
    {
        $user = User::findOrFail($request->id);

        $user->status = !$user->status;
        $user->save();

    }

    public function changeVideoStatus(Request $request)
    {
        $video = Video::findOrFail($request->id);
        $video->status = !$video->status;
        $video->save();
        return $video;
        // return \redirect()->back();
    }
    
    public function changeCommentStatus(Request $request)
    {
        $comment = Comment::findOrFail($request->id);
        $comment->status = !$comment->status;
        $comment->save();
        // dd("hello");
        // return redirect()->back();
    }
}
