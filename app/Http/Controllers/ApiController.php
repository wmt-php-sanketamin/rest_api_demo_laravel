<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use TymonJWTAuthExceptionsJWTException;
use Hash;
use Validator;
use JWTAuth;
use DB;

use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;

use App\Models\Video;
use App\Models\User;

use App\Jobs\SendVerifyEmail;
use App\Traits\ApiResponse;
use App\Utils\AppConstant;
use App\Http\Requests\CreateApiUserRequest;
use App\Http\Requests\ChangePassRequest;
use App\Http\Requests\VideoUploadRequest;



class ApiController extends Controller
{
    use ApiResponse;


    public function userRegister(CreateApiUserRequest $request){

        try {

            $validated = $request->validated();
            
        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        } catch (\Exception $e) {
            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
        if($request->hasFile('avtar')){
                
            $file = $request->file('avtar');
            $file_name = $file->getClientOriginalName();
            $path = $request->file('avtar')->storeAs(
                'public/images',$file_name
            );
        }
        $user = new User;
      
          $user->name = $request->name;
          $user->avtar = 'storage/images/'.$file_name;
          $user->email = $request->email;
          $user->password = bcrypt($request->password);
      
        $user->save();

        $this->setMeta('status', AppConstant::STATUS_OK);
        $this->setMeta('message', 'Account '. $user->name . ' with email '. $user->email . ' was created');
        return response()->json($this->setResponse(), AppConstant::OK);
      
    }

    public function userLogin(Request $request){
        
        try {
            $input = $request->all();
            $jwt_token = null;
            $email = $input['email'];
            $pwd = $input['password'];

            $user = User::where(['email' => $email ,'status' => AppConstant::STATUS_ACTIVE])->first();
            
            if(!$user){
                $this->setMeta('message', __('messages.user.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }
            
            $claim_credentials = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            ];

            $jwt_token = JWTAuth::claims($claim_credentials)->attempt($input);

            $token_details = $this->createUserTokenObj($jwt_token);
            
            if (!$jwt_token) {
                $this->setMeta('message', __('messages.user.invalidCredentials'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }
            
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setMeta('message', __('messages.user.login'));
            $this->setData('token_details', $token_details);
            $this->setData('user_data',$user);
            return response()->json($this->setResponse(), AppConstant::OK);

        }catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        } catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
        
    }

    public function userLogout(Request $request){

        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));
            JWTAuth::invalidate(JWTAuth::parseToken());
            auth()->logout();

        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        } catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
        $this->setMeta('message', __('messages.user.logout'));
        $this->setMeta('status', AppConstant::STATUS_OK);
        return response()->json($this->setResponse(),AppConstant::OK);
        
    }

    public function userEdit(Request $request){


        $name = $request->name;

        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();

            User::where('id','=', $token_data['id'])->update(['name'=>$name]);

            $user_1 = User::where('id','=', $token_data['id'])->first();

            $this->setMeta('message', __('messages.user.edit'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            $this->setData('edited_user_data',$user_1);
            return response()->json($this->setResponse(),AppConstant::OK);

        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        } catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
        
    }

    public function userDelete(Request $request){

        try {
            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();

            $user = User::find($token_data['id']);
            $user->delete();

            $this->setMeta('message', __('messages.user.delete'));
            $this->setMeta('status', AppConstant::STATUS_OK);
            return response()->json($this->setResponse(),AppConstant::OK);
            

        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        } catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
    }

    public function changePass(ChangePassRequest $request){

        try {
            $validated = $request->validated();

            $token = str_replace('Bearer ', '', $request->header('Authorization'));

            $token_data = JWTAuth::getPayload($token)->toArray();
            $user = User::find($token_data['id']);

            $user_pwd = auth()->user()->password;
            $user_id = $token_data['id'];
            

            if(!Hash::check($request['current_password'], $user_pwd)){

                $this->setMeta('message', __('messages.password.currentpassNotMatch'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }
            if(Hash::check($request['new_password'], $user_pwd)){
                $this->setMeta('message', __('messages.password.newCurrentNotSame'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

            }
                User::where('id','=', $user_id)->update(['password'=>bcrypt($request['new_password'])]);

                $this->setMeta('message', __('messages.password.changed'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                return response()->json($this->setResponse(),AppConstant::OK);
            
        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
    }


    public function ForgotPass(Request $request){

        try {
            
            $input = $request->all();
            $email = $input['email'];
            $user = User::where('email',$email)->first();
            if($user){

                $input['token'] = Str::random(60);
                DB::table('password_resets')->insert(
                    ['email' => $input['email'], 'token' => $input['token'],'created_at' => Carbon::now()]
                );
                $tokenData = DB::table('password_resets')->where('email', $request->email)->first();
                
                $input['url'] = route('password.reset', ['token' => $input['token']]);
                // dd($input['url']);
                SendVerifyEmail::dispatch($input);

                $this->setMeta('message', __('messages.password.changePassLink'));
                $this->setMeta('status', AppConstant::STATUS_OK);
                return response()->json($this->setResponse(),AppConstant::OK);

            }else{
                $this->setMeta('message',  __('messages.user.notFound'));
                $this->setMeta('status', AppConstant::STATUS_FAIL);
                return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);
            }             

        } catch (QueryException $e) {

            $this->setMeta('message', __('messages.somethingWrong'));
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }catch (\Exception $e) {

            $this->setMeta('message', $e->getMessage());
            $this->setMeta('status', AppConstant::STATUS_FAIL);
            return response()->json($this->setResponse(), AppConstant::INTERNAL_SERVER_ERROR);

        }
    }

    protected function createUserTokenObj($token)
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL(),
        ];
    }

}
