<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Session;
// use App\Http\Controllers\Redirect;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
// Use Redirect;
use App\Http\Requests\AdminLoginRules;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['web','auth.admin'])->except('login', 'logout');
        // $this->middleware('auth.admin')->except('logout');
    }
    
    public function showLoginForm()
    {
        return view('auth.login');
    }

    protected function guard(){
        return Auth::guard('admin');
    }

    public function login(AdminLoginRules $request)
    {
        // dd($request->all());
        $validated = $request->validated();
        // $remember = $request->input("remember");
        $remember_me  = ( !empty( $request->remember ) )? TRUE : FALSE;
        // dd($remember_me);
        if (Auth::guard('admin')->attempt(['email' => $validated['email'], 'password' => $validated['password']],$remember_me)) {
            $admin = Auth::guard('admin')->user();
            return Redirect::route('admin')->with(['admin' => $admin]);
        } else{
            Session::flash('message', __('messages.admin.notFound'));
        }
            return back()->withInput($request->only('email', 'remember'));
    }
    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        if ($response = $this->loggedOut($request)) {
            return $response;
        }
        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect('/login');
    }
}
