<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Exception;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtAuthMiddleware extends BaseMiddleware
{
    use ApiResponse;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (TokenExpiredException $e) {
            $this->setMeta('status', false);
            $this->setMeta('message', __('messages.tokenExpired'));
            return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (TokenInvalidException $e) {
            $this->setMeta('status', false);
            $this->setMeta('message', __('messages.tokenInvalid'));
            return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (JWTException $e) {
            $this->setMeta('status', false);
            $this->setMeta('message', __('messages.tokenRequired'));
            return response()->json($this->setResponse(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return $next($request);
    }
}