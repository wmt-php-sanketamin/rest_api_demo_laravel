
            function CommentSwal(status, id){
                // dd("hello");
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    changeCommentStatus(status, id);
                } else {
                    swal("Comment file is safe!");
                }
                });
                
            }
            
            function changeCommentStatus(status, id){
                console.log(status);
                console.log(id);
                $.ajax({
                    url: '{{route('+changeCommentStatus+')}}' ,
                    type: 'POST',
                    data : { 
                            "status": status,
                            "id": id,
                            "_token": "{{ csrf_token() }}", },
                    error: function() {
                        console.log('Something is wrong');
                    },
                    success: function(data) {
                        window.location.href = "{{route('getcomments')}}";
                    }
                });
            }