<!DOCTYPE html>
<html lang="en">

	<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/default/full/login_tabbed.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 29 Mar 2019 10:46:33 GMT -->
	<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

		<!-- Global stylesheets -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
		<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/assets/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/assets/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/assets/layout.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/assets/components.min.css') }}" rel="stylesheet" type="text/css">
		<link href="{{ asset('css/assets/colors.min.css') }}" rel="stylesheet" type="text/css">
		<!-- /global stylesheets -->

		<!-- Core JS files -->
		<script src="{{ asset('js/main/jquery.min.js') }}"></script>
		<script src="{{ asset('js/main/bootstrap.bundle.min.js') }}"></script>
		<script src="{{ asset('js/plugins/loaders/blockui.min.js') }}"></script>

		<!-- Theme JS files -->
		<script src="{{ asset('js/demo_pages/login.js') }}"></script>
		<script src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
		<script src="{{ asset('js/assets/app.js') }}"></script>
		<!-- /theme JS files -->

	</head>

	<body>

		<!-- Page content -->
		<div class="page-content login-cover">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content d-flex justify-content-center align-items-center">
					
					<!-- Login form -->
					<form class="login-form wmin-sm-400" method="POST" action="{{ route('login') }}">
						@csrf
						
						<div class="card mb-0">
							<div class="tab-content card-body">
								<div class="tab-pane fade show active" id="login-tab1">
									<div class="text-center mb-3">
										<i class="fas fa-user-lock fa-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i>
										<h5 class="mb-0">Login to your account</h5>
										<span class="d-block text-muted">Your credentials</span>
									</div>
									@if(Session::has('message'))
										<p class="alert alert-danger }}">{{ Session::get('message') }}</p>
									@endif
									<div class="form-group form-group-feedback form-group-feedback-left">
										
										<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Username" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

										@error('email')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<div class="form-control-feedback">
											<i class="icon-user text-muted"></i>
										</div>
									</div>

									<div class="form-group form-group-feedback form-group-feedback-left">
										<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

										@error('password')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
										<div class="form-control-feedback">
											<i class="icon-lock2 text-muted"></i>
										</div>
									</div>

									<div class="form-group d-flex align-items-center">
										<div class="form-check mb-0">
											<label class="form-check-label">
												<input class="form-check-input" type="checkbox" name="remember" id="remember">

												Remember
											</label>
										</div>
										@if (Route::has('password.request'))
											<a class="btn btn-link" href="{{ route('password.request') }}">
												{{ __('Forgot Your Password?') }}
											</a>
										@endif
									</div>

									<div class="form-group">
										<button type="submit" class="btn btn-primary btn-block">Sign in</button>
									</div>

								</div>
							</div>
						</div>
					</form>
					<!-- /login form -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</body>

	<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/default/full/login_tabbed.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 29 Mar 2019 10:46:33 GMT -->
</html>
