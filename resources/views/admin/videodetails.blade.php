@extends('layouts.admin')

@section('content')

           
    <!-- /basic datatable -->
    <div class="row justify-conetnt-center">
        <div class="card card1 col-md-12">
            <div class="card-header header-elements-inline">
                <h3 class="card-title fw-bold">Video Details</h3>
                <a class="btn btn-secondary ml-auto" href="{{route('getvideos')}}" role="button"><i class="fas fa-arrow-left"></i> Back</a></td>
            </div>
            
            <div class="row d-flex">
                <div class="col-md-4 img1">
                    <img src="../../storage/flat/13.png"  alt="...">
                </div>
                <div class="col-md-8 pt-0">
                    <div class="card-body">
                    <form>
                        <div class="form-group row">
                            <label for="Name" class="col-sm-2 text-dark col-form-label">Name: </label>
                            <div class="col-sm-10">
                                <p>{{$reported->VideoName}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Path" class="col-sm-2 text-dark col-form-label">Path: </label>
                            <div class="col-sm-10">
                                <p>{{$reported->video_path}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="About" class="col-sm-2 text-dark col-form-label">About: </label>
                            <div class="col-sm-10">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Likes" class="col-sm-2 text-dark col-form-label">Likes: </label>
                            <div class="col-sm-10">
                                <p>{{$reported->vote_count}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Likes" class="col-sm-2 text-dark col-form-label">Comments: </label>
                            <div class="col-sm-10">
                                <p>{{$reported->comments_count}}</p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Status" class="col-sm-2 text-dark col-form-label">Status: </label>
                            <div class="col-sm-10">
                                <p>
                                @if($reported->status == 1)
                                    <span class="badge bg-success mr-5">Active</span>
                                    <button type="button" class="btn btn-secondary m-0 pr-4" onclick="VideoSwal('{{$reported->id}}')">Deactivate</button>
                                @else
                                    <span class="badge bg-danger mr-5">Reported</span>
                                    <button type="button" class="btn btn-secondary m-0 pr-4" onclick="VideoSwal('{{$reported->id}}')">Activate</button>
                                @endif
                                </p>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    
    <div class="row">
        <div class="col-md-7">
            <div class="card border-light">
                <div class="card-header header-elements-inline p-0">
                    <h3 class="card-title fw-bold">Video Reported By:</h3>
                </div>
                <table class="table datatable-basic">
                    <thead class="bg-dark">
                        <tr>
                            <th>#</th>
                            <th>UserID</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; ?>
                        @foreach($reported->reports as $report)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td>{{ $report->user_id }}</td>
                            @if($report->status == 1)
                            <td><span class="badge bg-success">Active</span></td>
                            @else
                            <td><span class="badge bg-danger">Reported</span></td>
                            @endif
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    <script type="text/javascript">
    function VideoSwal(id){
                swal({
                    title: "Are you sure?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    changeVideoStatus(id);
                } else {
                    swal("Video file is safe!");
                }
                });
            }
            function changeVideoStatus(id){
                $.ajax({
                    url: '{{route('changeVideoStatus')}}',
                    type: 'POST',
                    data : { 
                            "id": id,
                            "_token": "{{ csrf_token() }}", },
                    error: function() {
                        console.log('Something is wrong');
                    },
                    success: function(data) {
                        console.log(data.status);
                        if(data.status == true){
                            $(".badge.mr-5").addClass("bg-success").removeClass("bg-danger").text("Active");
                            $(".btn.btn-secondary.m-0.pr-4").text("Deactivate");
                        }
                        else{
                            $(".badge.mr-5").addClass("bg-danger").removeClass("bg-success").text("Reported");
                            $(".btn.btn-secondary.m-0.pr-4").text("Activate");
                        }
                    }
                });
            }
        </script>
@endsection
