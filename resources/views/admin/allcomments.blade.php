@extends('layouts.admin')

@section('content')

    <!-- Basic datatable -->
    <div class="card border-light">
		<div class="card-header header-elements-inline">
			<h3 class="card-title fw-bold">All Comments</h3>
            <a class="btn btn-secondary ml-auto" href="{{route('getvideos')}}" role="button"><i class="fas fa-arrow-left"></i> Back</a></td>
		</div>
		<table class="table comments_table">
			<thead class="bg-dark">
				<tr>
                    <th>Comment_ID</th>
					<th>User_ID</th>
					<th>Video_ID</th>
					<th>Comment</th>
					<th>Status</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
    <!-- /basic datatable -->
	<script type="text/javascript">

	$(document).ready(function(){

		$('.comments_table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('getcomments') }}",
			columns: [
				{data: 'id', name: 'id'},
				{data: 'user_id', name: 'user_id'},
				{data: 'video_id', name: 'video_id'},
				{data: 'body',name: 'body'},
				{
					data: 'status',
					name: 'status',
					render(data){
						if(data == 1){
							return "<span class='badge bg-success'>Active</span>";
						}else{
							return "<span class='badge bg-danger'>Reported</span>";
						}
					}
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				},
			]
		});
	});

	function CommentSwal(id){
		swal({
			title: "Are you sure?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		})
		.then((willDelete) => {
			if (willDelete) {
				changeCommentStatus(id);
			} else {
				swal("Comment file is safe!");
			}
		});
    }
    function changeCommentStatus(id){
        $.ajax({
            url: '{{route('changeCommentStatus')}}',
            type: 'POST',
            data : { 
                    "id": id,
                    "_token": "{{ csrf_token() }}", },
            error: function() {
                console.log('Something is wrong');
            },
            success: function(data) {
                var table = $('.comments_table').DataTable(); 
                table.ajax.reload( null, false );
            }
        });
    }
	</script>
@endsection