@extends('layouts.admin')

@section('content')

    <!-- Basic datatable -->
    <div class="card border-light">
		<div class="card-header header-elements-inline">
			<h3 class="card-title fw-bold">All Users</h3>
			<a class="btn btn-secondary ml-auto" href="{{route('admin')}}" role="button"><i class="fas fa-arrow-left"></i> Back</a></td>
		</div>
		<table class="table users_table">
			<thead class="bg-dark">
				<tr>
					<th>UserID</th>
					<th>Avtar</th>
					<th>Full Name</th>
					<th>Email</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
    <!-- /basic datatable -->
	<script type="text/javascript">

	$(document).ready(function(){
		$('.users_table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('getusers') }}",
			columns: [
				{data: 'id', name: 'id'},
				{
					data: 'avtar', 
					name: 'avtar',
					render(data){
						return "<img src='../../"+data+"'>";
					}
				},
				{data: 'name', name: 'name'},
				{data: 'email', name: 'email'},
				{
					data: 'status',
					name: 'status',
					render(data){
						if(data == 1){
							return "<span class='badge bg-success'>Active</span>";
						}else{
							return "<span class='badge bg-danger'>Reported</span>";
						}
					}
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				},
			]
		});
	});

	function UserSwal(id){
        swal({
            title: "Are you sure?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
			if (willDelete) {
				changeUserStatus(id);
			} else {
				swal("User file is safe!");
			}
			});
		}
		function changeUserStatus(id){
			$.ajax({
				url: '{{route('changeUserStatus')}}',
				type: 'POST',
				data : { 
						"id": id,
						"_token": "{{ csrf_token() }}", },
				error: function() {
					console.log('Something is wrong');
				},
				success: function(data) {
					var table = $('.users_table').DataTable(); 
					table.ajax.reload( null, false );
				}
			});
		}
	</script>
@endsection

