@extends('layouts.admin')

@section('content')

    <!-- Basic datatable -->
    <div class="card border-light">
		<div class="card-header header-elements-inline p-0">
			<h3 class="card-title fw-bold">All Users</h3>
		</div>
		<table class="table datatable-basic">
			<thead class="bg-dark">
				<tr>
					<th>#</th>
					<th>UserID</th>
					<th>Full Name</th>
					<th>Email</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>12</td>
					<td>51</td>
					<td>123</td>
					<td><span class="badge bg-success">Active</span></td>
					<td><a class="btn btn-secondary btn-sm" href="#" role="button"><i class="fas fa-user-minus"></i> Deactivate</a></td>
					
				</tr>
			</tbody>
		</table>
	</div>
    <!-- /basic datatable -->
    

@endsection