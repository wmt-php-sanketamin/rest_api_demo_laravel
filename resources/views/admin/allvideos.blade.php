@extends('layouts.admin')

@section('content')

    <!-- Basic datatable -->
    <div class="card border-light">
		<div class="card-header header-elements-inline">
			<h3 class="card-title fw-bold">All Videos</h3>
			<a class="btn btn-secondary ml-auto" href="{{route('admin')}}" role="button"><i class="fas fa-arrow-left"></i> Back</a></td>
		</div>
		<table class="table videos_table">
			<thead class="bg-dark">
				<tr>
                    <th>VideoID</th>
					<th>UserID</th>
					<th>Video_Name</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
    <!-- /basic datatable -->
	<script type="text/javascript">

	$(document).ready(function(){
		$('.videos_table').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('getvideos') }}",
			columns: [
				{data: 'id', name: 'id'},
				{data: 'user_id', name: 'user_id'},
				{data: 'video_path',name: 'video_path'},
				{
					data: 'status',
					name: 'status',
					render(data){
						if(data == 1){
							return "<span class='badge bg-success'>Active</span>";
						}else{
							return "<span class='badge bg-danger'>Reported</span>";
						}
					}
				},
				{
					data: 'action',
					name: 'action',
					orderable: false,
					searchable: false
				},
			]
		});

	});

	
	</script>
@endsection