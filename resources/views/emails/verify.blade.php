@component('mail::message')
# Introduction

Dear, Verify Your Email Address.
@if (session('resent'))
<div class="alert alert-success" role="alert">
{{ __('A fresh verification link has been sent to your email address.') }}
</div>
@endif
<a class="btn btn-primary" href="{{$user['url']}}" role="button">Link</a>
@component('mail::button', ['url' => '#'])
Click Here
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
