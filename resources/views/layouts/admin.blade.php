<html lang="en">

<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/default/full/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 29 Mar 2019 10:14:45 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Laravel Admin Demo</title>
        
        <!-- Global stylesheets -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/assets/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/assets/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/assets/layout.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/assets/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/assets/colors.min.css') }}" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script src="{{ asset('js/main/jquery.min.js') }}"></script>
        <script src="{{ asset('js/main/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('js/plugins/loaders/blockui.min.js') }}"></script>
        <!-- /core JS files -->

        <!-- theme JS files -->
        <script src="{{ asset('js/plugins/visualization/d3/d3.min.js') }}"></script>
        <script src="{{ asset('js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
        <script src="{{ asset('js/plugins/forms/styling/switchery.min.js') }}"></script>
        <script src="{{ asset('js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
        <script src="{{ asset('js/plugins/ui/moment/moment.min.js') }}"></script>
	    <script src="{{ asset('js/plugins/pickers/daterangepicker.js') }}"></script>
        <script src="{{ asset('js/plugins/notifications/sweet_alert.min.js') }}"></script>
        <script src="{{ asset('js/plugins/forms/styling/uniform.min.js') }}"></script>
        <script src="{{ asset('js/demo_pages/extra_sweetalert.js') }}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	    <script src="{{ asset('js/demo_pages/login.js') }}"></script>
        <script src="{{ asset('js/plugins/tables/datatables/datatables.min.js') }}"></script>
        <!-- <script src="{{ asset('js/plugins/forms/selects/select2.min.js') }}"></script> -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        
        <script src="{{ asset('js/assets/app.js') }}"></script>
        <script src="{{ asset('js/demo_pages/dashboard.js') }}"></script>
        <script src="{{ asset('js/demo_pages/datatables_basic.js') }}"></script>
        <!-- /theme JS files -->

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>


        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>

    <body>
        <div class="navbar navbar-expand-md navbar-dark">
            <div class="navbar-brand">
                <a href="index.html" class="d-inline-block">
                    <img src="../../storage/brands/logo_light.png" alt="">
                </a>
            </div>

            <div class="d-md-none">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                    <i class="icon-tree5"></i>
                </button>
                <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                    <i class="icon-paragraph-justify3"></i>
                </button>
            </div>

            <div class="navbar-nav ml-auto">
                <div class="nav-item dropdown dropdown-user">
                    <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                        <img src="../../storage/images/face24.jpg" class="rounded-circle mr-2" height="34" alt="">
                        <span>{{$admin->name}}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="btn disabled"><i class="icon-user-plus"></i> My profile</a>
                        <a href="#" class="btn disabled"><i class="icon-cog5"></i> Account settings</a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        <i class="icon-switch2"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="page-content">
            <!-- Main sidebar -->
            <div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

                <!-- Sidebar mobile toggler -->
                <div class="sidebar-mobile-toggler text-center">
                    <a href="#" class="sidebar-mobile-main-toggle">
                        <i class="icon-arrow-left8"></i>
                    </a>
                    Navigation
                    <a href="#" class="sidebar-mobile-expand">
                        <i class="icon-screen-full"></i>
                        <i class="icon-screen-normal"></i>
                    </a>
                </div>
                <!-- /sidebar mobile toggler -->

                <!-- Sidebar content -->
                <div class="sidebar-content">
                    <!-- User menu -->
                    <div class="sidebar-user">
                        <div class="card-body">
                            <div class="media">
                                <div class="mr-3">
                                    <a href="#"><img src="../../storage/images/face24.jpg" width="38" height="38" class="rounded-circle" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-title font-weight-semibold">{{$admin->name}}</div>
                                    <div class="font-size-xs opacity-50">
                                        <i class="icon-pin font-size-sm"></i> &nbsp;Ahmedabad, India
                                    </div>
                                </div>

                                <div class="ml-3 align-self-center">
                                    <a href="#" class="text-white btn disabled"><i class="icon-cog3"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /user menu -->

                    <!-- Main navigation -->
                    <div class="card card-sidebar-mobile">
                        <ul class="nav nav-sidebar" data-nav-type="accordion">

                            <!-- Main -->
                            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
                            <li class="nav-item">
                                <a href="{{route('admin')}}" class="nav-link active">
                                    <i class="icon-home4"></i>
                                    <span>
                                        Dashboard
                                    </span>
                                </a>
                            </li>
                            <li class="nav-item nav-item-submenu">
                                <a href="#" class="nav-link"><i class="icon-users"></i> <span>Users</span></a>
                                
                                <ul class="nav nav-group-sub" data-submenu-title="Users">
                                    <li class="nav-item"><a href="{{route('getusers')}}" class="nav-link active">Users List</a></li>
                                    <li class="nav-item"><a href="{{route('getvideos')}}" class="nav-link">Users Video</a></li>
                                    <li class="nav-item"><a href="{{route('getcomments')}}" class="nav-link">Users Comments</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /main navigation -->

                </div>
                <!-- /sidebar content -->
            </div>
            <!-- /main sidebar -->
        

            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Content area -->
                <div class="content">

                    @yield('content')
            
                </div>
                <!-- /content area -->

                <!-- Footer -->
                <div class="navbar navbar-expand-lg navbar-light mt-auto">
                    <div class="text-center d-lg-none w-100">
                        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                            <i class="icon-unfold mr-2"></i>
                            Footer
                        </button>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar-footer">
                        <span class="navbar-text">
                            &copy; 2015 - 2018. <a href="#">Limitless Web App Kit</a> by <a href="http://themeforest.net/user/Kopyov" target="_blank">Eugene Kopyov</a>
                        </span>
                        <ul class="navbar-nav ml-lg-auto">
                            <li class="nav-item"><a href="https://kopyov.ticksy.com/" class="navbar-nav-link" target="_blank"><i class="icon-lifebuoy mr-2"></i> Support</a></li>
                            <li class="nav-item"><a href="http://demo.interface.club/limitless/docs/" class="navbar-nav-link" target="_blank"><i class="icon-file-text2 mr-2"></i> Docs</a></li>
                            <li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-cart2 mr-2"></i> Purchase</span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- /footer -->
            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->
        <script type="text/javascript">

            

        </script>
    </body>
</html>

