<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'tokenExpired' => 'Token Expired',
    'tokenInvalid' => 'Invalid Token',
    'tokenRequired' => 'Token Required',
    'somethingWrong' => 'Something Wrong',
    'voteSuccess' => 'Vote Successfully Updated..!',
    'updateFlag' => 'Flag Updated Successfully..',
    
    'admin' => [
        'notFound' => 'Invalid Username or Password',
    ],
    'user' => [
        'invalidCredentials' => 'Invalid Username or password',
        'login' => 'User Logged In Successfully',
        'logout' => 'User Logged Out Successfully.',
        'edit' => 'User Edited Successfully.',
        'delete' => 'User Deleted Successfully',
        'notFound' => 'User Not Found',
    ],
    
    'password' => [
        'currentpassNotMatch' => 'Current Password Does not Match',
        'newCurrentNotSame' => 'New Password Can not be same as Current Password',
        'changed' => 'Password Change Successfully',
        'changePassLink' => 'Change Password Link Send Successfully',
    ],

    'video' => [
        'upload' => 'Video Uploaded Successfully',
        'notFound' => 'Video Not Found',
        'allVideos' => 'All Videos',
    ],

    'comment' => [
        'commentSuccess' => 'Woohooo...Comment add Successfullyy...!',
        'notFound' => 'No Comment Found',
    ],

];
